Now, that we have explained how to the OpenAPI documentation works and
listed the available APIs on data.europa.eu, we can provide some example
API requests for the search API.

Remember, the URL for the search API is
https://data.europa.eu/api/hub/search/. The simplest possible search is
to just request all metadata without any further parameters that would
reduce the search results. Looking at the documentation of the search
API this can be achieved by just adding the path "search". Our first
example API call looks like this:


<a href="https://data.europa.eu/api/hub/search/" target="_top">https://data.europa.eu/api/hub/search/</a>

It provides a limited list of all metadata that is stored in
data.europa.eu. This list is limited, because data.europa.eu stores too
much metadata and this cannot be transmitted in one single request. If
you would like to get all metadata available, multiple requests would be
necessary. Here, a method called paging comes into play. With paging you
can split the results in "pages" and simply go through them. The
examples below will showcase how to retrieve three pages of metadata via
the search API:

<a href="https://data.europa.eu/api/hub/search/search?page=0&limit=10" target="_top">https://data.europa.eu/api/hub/search/search?page=0&limit=10</a>

<a href="https://data.europa.eu/api/hub/search/search?page=1&limit=10" target="_top">https://data.europa.eu/api/hub/search/search?page=1&limit=10</a>

<a href="https://data.europa.eu/api/hub/search/search?page=2&limit=10" target="_top">https://data.europa.eu/api/hub/search/search?page=2&limit=10</a>


Note, that these examples introduce the two parameters page and limit.
Basically, the API call is asking for page 0, 1, 2 of the search results
with maximum 10 results per page. The page parameter selects the
requested page and limit sets the maximum number of results to be
retrieved per page.

For a last example we will add a search term to the request. The
following request shall return page 0 with a maximum of 100 results that
match with the search term "water":

<a href="https://data.europa.eu/api/hub/search/search?page=0&limit=100&q=water" target="_top">https://data.europa.eu/api/hub/search/search?page=0&limit=100&q=water</a>



Please consult the above referenced OpenAPI documentations for more
information on how to use out APIs.
