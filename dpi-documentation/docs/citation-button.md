The citation button will help you retrieve citations for datasets
without much effort and will guarantee their correctness.

**Where can you find it?**

Just go to the page of the dataset you want to cite. You will find the
button on the grey menu, last position on the right.

![](./images/media/Cite-menu.PNG)
**How can you use it?**

Just click on 'Cite' and choose your preferred citation style. You can
choose among the following: [EU Data
Citation](https://data.europa.eu/doi/10.2830/59387), APA, Harvard,
Vancouver.

![](./images/media/Cite.PNG)


Once you click on your preferred style, the citation will display on the
screen.

![](./images/media/image80.png)

Copy to clipboard and it is ready -- you can paste your citation
wherever you need.
