# Contact

## Contacting the portal

 **By online contact form**

You can contact us using our contact form, which allows you to choose
the issue type (request for information, feedback and suggestions for
improvement, error report, question on copyright/reuse of data,
question on SPARQL/API, question on specific dataset, suggestion for a
new dataset, other questions)

Simply click on <https://data.europa.eu/en/feedback/form>


## Contacting the EU

**In person**

All over the European Union there are hundreds of Europe Direct
information centres. You can find the address of the centre nearest
you at: <https://europa.eu/european-union/contact_en>

**On the phone or by email**

Europe Direct is a service that answers your questions about the
European Union. You can contact this service:

-   by freephone: 00 800 6 7 8 9 10 11 (certain operators may charge for
    these calls),

-   at the following standard number: +32 22999696, or

-   by email via: <https://europa.eu/european-union/contact_en>

## Finding information about the EU

Online

Information about the European Union in all the official languages of
the EU is available on the Europa website at:
<https://europa.eu/european-union/index_en>

**EU publications**

You can download or order free and priced EU publications from:
<https://op.europa.eu/en/publications> Multiple copies of free
publications may be obtained by contacting Europe Direct or your local
information centre (see
<https://europa.eu/european-union/contact_en>).

**EU law and related documents**

For access to legal information from the EU, including all EU law
since 1951 in all the official language versions, go to EUR-Lex at:
[http://eur-lex.europa.eu](https://eur-lex.europa.eu/)

**Open data from the EU**

The official portal for European data (<https://data.europa.eu/en>)
provides access to datasets from the EU. Data can be downloaded and
reused for free, for both commercial and non-commercial purposes.
