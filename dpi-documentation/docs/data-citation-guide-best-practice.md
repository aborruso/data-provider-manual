[*Data citation: A guide to best practice*](https://doi.org/10.2830/59387) is a study and collection of
best practices and recommendations in the field of data citation. It was
published by the OP in 2022. It is divided into 3 parts.

Part 1: issues in data citation, general recommendations.

Part 2: recommendations on specific formats and other elements for data
citation.

Part 3: issues in data citation, general recommendations.

Below you will find the annexes that contain useful quick reference
materials like checklists, diagrams and examples.

If you are interested in this topic, you can find a dedicated training
in [data.europa
academy](https://data.europa.eu/en/academy/data-citation-guide-best-practice-data-citation-citation-interoperability)
where the author, Paul Jessop, presents the study in an online seminar.

**Useful links:**

[*Data citation: A guide to best
practice*](https://doi.org/10.2830/59387)

[Recording of the online seminar 'Data
citation'](https://youtu.be/zgy8Lwww44U)

[Slides from the online seminar 'Data
citation'](https://webgate.ec.europa.eu/fpfis/wikis/download/attachments/963871680/Data%20citation%20online%20presentation%20v4.pdf?version=1&modificationDate=1635507928754&api=v2)

[Data citation training on data.europa
academy.](https://data.europa.eu/en/academy/data-citation-guide-best-practice-data-citation-citation-interoperability)

In the following subsections you will find quick-reference material for
data citation.

### Checklist (long) for footnotes or reference-list entries

The following steps should be a checklist when creating a citation
that will appear in a footnote or reference-list entry.

The key initial step is to refer to the metadata of the dataset that
has been used. This may include a 'cite this dataset as' function.
This will provide most, or all of the information needed, though it
may appear in a slightly different format and some editing work may be needed.

Otherwise, the metadata should include all the information needed,
though sometimes a little 'research' will be needed to locate some of
it.

![data-citation-checklist](./images/media/data-citation-checklist.png)


* [https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/dcat-application-profile-data-portals-europe](https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/dcat-application-profile-data-portals-europe)

* <http://publications.europa.eu/code/en/en-000100.htm>

*Source:* Publications Office, Jessop, P., *Data citation: A guide to
best practice*, 2022, <https://data.europa.eu/doi/10.2830/59387>

### Checklist (short) for footnotes or reference- list entries

![](./images/media/image75.png)

Elements:

![data-citation-checklist](./images/media/data-citation-elements.png)

*Source:* Publications Office, Jessop, P., *Data citation: A guide to
best practice*, 2022, <https://data.europa.eu/doi/10.2830/59387>

### Diagram for footnote and reference citation style with date format compliant with DCAT-AP

![](./images/media/image76.png)

Elements:

![data-citation-checklist](./images/media/data-citation-elements.png)

[*Source:* Publications Office, Jessop, P., *Data citation: A guide to]()
best practice*, 2022, <https://data.europa.eu/doi/10.2830/59387>

### Diagram for footnote and reference citation style with date format compliant with the Interinstitutional Style Guide

![](./images/media/image77.png)

Elements:

![data-citation-checklist](./images/media/data-citation-elements.png)

*Source:* Publications Office, Jessop, P., *Data citation: A guide to
best practice*, 2022, <https://data.europa.eu/doi/10.2830/59387>

### Examples of citations with date format compliant with DCAT-AP

European Commission, Directorate-General for Financial Stability,
Financial Services and Capital Markets Union, 'Consolidated list of
persons, groups, and entities subject to EU financial sanctions,' 2016
(updated 2020-10-23), accessed 2021-10-01,
<http://data.europa.eu/88u/dataset/consolidated-list-of-persons-groups-and-entities-subject-to-eu-financial-sanctions-fisma>

European Commission, Directorate-General for Internal Market, Industry,
Entrepreneurship and SMEs (Small Medium Enterprises), 'Tenders
Electronic Daily (TED) (csv subset) -- public procurement notices,'
version 3.3, 2015 (updated 2020-11-22),
<http://data.europa.eu/88u/dataset/ted-csv>

European Commission, Directorate-General for Informatics, 'National
Interoperability Framework Observatory (NIFO) -- Digital Public
Administration factsheets 2020', accessed 2021-10-01,
<http://data.europa.eu/doi/10.2906/100105103105116/1>

European Commission, European Chemicals Agency, 'Mammalian toxicokinetic
database (MamTKDB) 1.0,' version 1.0, 2021, accessed 2021-10-01,
<http://data.europa.eu/88u/dataset/mammalian-toxicokinetic-database-mamtkdb-1-0>

European Commission, European Centre for Disease Prevention and Control,
'COVID-19 coronavirus data -- daily (up to 14 December 2020)', accessed
2021-10-01,
<http://data.europa.eu/88u/dataset/covid-19-coronavirus-data-daily-up-to-14-december-2020>

European Commission, Directorate-General for Internal Market, Industry,
Entrepreneurship and SMEs, 'Cosmetic ingredient database (Cosing) --
List of substances prohibited in cosmetic products,' 2016 (updated
2018-12-14),
[http://data.europa.eu/88u/dataset/cosmetic-ingredient-database-2-list-of-substances-prohibited-in-](http://data.europa.eu/88u/dataset/cosmetic-ingredient-database-2-list-of-substances-prohibited-in-cosmetic-products)
[cosmetic-products](http://data.europa.eu/88u/dataset/cosmetic-ingredient-database-2-list-of-substances-prohibited-in-cosmetic-products)

'European database of suspected adverse drug reaction reports
(EudraVigilance),' European Commission, European Medicines Agency, 2015
(updated 2019-01-10),
<http://data.europa.eu/88u/dataset/suspected-adverse-drug-reaction-reports>

European Commission, Joint Research Centre, 'European Soil Database v2.0
(vector and attribute data),' 2021, accessed 2021-10-01,
<http://data.europa.eu/89h/jrc-esdac-1>

Sevini, F. and Arnés-Novau, X., 'Export Control Handbook for Chemicals,'
version 2021 edition, European Commission, Joint Research Centre (JRC),
2021, accessed 2021-10-01,
<http://data.europa.eu/89h/8f0d4fa2-110d-4c05-b2a8-ecd3addbf16b>

Nijs, W. and Ruiz, P., '01_JRC-EU-TIMES Full model', European
Commission, Joint Research Centre, 2019,
<http://data.europa.eu/89h/8141a398-41a8-42fa-81a4-5b825a51761b>

'AMECO -- ECFIN annual macroeconomic database,' European Commission,
Directorate- General for Economic and Financial Affairs, (updated
2020-11-05),
[http://data.europa.](http://data.europa.eu/88u/dataset/ameco)
[eu/88u/dataset/ameco](http://data.europa.eu/88u/dataset/ameco)

European Commission, Directorate-General for Energy, 'Energy modelling
-- EU Reference Scenario 2016',
<http://data.europa.eu/88u/dataset/energy-modelling>

European Commission, Eurostat, 'Airport traffic data by reporting
airport and airlines' (avia_tf_apal), most recent data 2021-09-01,
[https://ec.europa.eu/eurostat/](https://ec.europa.eu/eurostat/databrowser/view/avia_tf_apal/default/table?lang=en)
[databrowser/view/avia_tf_apal/default/table?lang=en](https://ec.europa.eu/eurostat/databrowser/view/avia_tf_apal/default/table?lang=en)

European Commission, Eurostat, 'Total length of motorways' (ttr00002),
accessed 2021-10-15,
[https://ec.europa.eu/eurostat/databrowser/view/ttr00002/default/](https://ec.europa.eu/eurostat/databrowser/view/ttr00002/default/table?lang=en)
[table?lang=en](https://ec.europa.eu/eurostat/databrowser/view/ttr00002/default/table?lang=en)

European Commission, Eurostat, 'Real GDP growth rate -- volume'
(tec00115), updated 2021-09-28,
[https://ec.europa.eu/eurostat/databrowser/view/tec00115/default/](https://ec.europa.eu/eurostat/databrowser/view/tec00115/default/table?lang=en)
[table?lang=en](https://ec.europa.eu/eurostat/databrowser/view/tec00115/default/table?lang=en)

Publications Office of the European Union, 'Country Named Authority
List,' 2009 (updated 2021-09-29),
[http://data.europa.eu/88u/dataset/country](https://data.europa.eu/euodp/data/dataset/country)

*Source:* Publications Office, Jessop, P., *Data citation: A guide to
best practice*, 2022, <https://data.europa.eu/doi/10.2830/59387>

### Examples of citations with date format compliant with the *Interinstitutional Style Guide*

European Commission, Directorate-General for Financial Stability,
Financial Services and Capital Markets Union, 'Consolidated list of
persons, groups, and entities subject to EU financial sanctions,' 2016
(updated 23 October 2020), accessed 1 October 2021,
[http://data.europa.eu/88u/dataset/consolidated-list-of-persons-groups-and-](http://data.europa.eu/88u/dataset/consolidated-list-of-persons-groups-and-entities-subject-to-eu-financial-sanctions-fisma)
[entities-subject-to-eu-financial-sanctions-fisma](http://data.europa.eu/88u/dataset/consolidated-list-of-persons-groups-and-entities-subject-to-eu-financial-sanctions-fisma)

European Commission, Directorate-General for Internal Market, Industry,
Entrepreneurship and SMEs, 'Tenders Electronic Daily (TED) (csv
subset) -- public procurement notices,' version 3.3, 2015 (updated
22 November 2020), <http://data.europa.eu/88u/dataset/ted-csv>

European Commission, Directorate-General for Informatics, 'National
Interoperability Framework Observatory (NIFO) -- Digital public
administration factsheets 2020', accessed 1 October 2021,
<http://data.europa.eu/doi/10.2906/100105103105116/1>

European Commission, European Chemicals Agency, 'Mammalian toxicokinetic
database (MamTKDB) 1.0,' version 1.0, 2021, accessed 1 October 2021,
[http://data.](http://data.europa.eu/88u/dataset/mammalian-toxicokinetic-database-mamtkdb-1-0)
[europa.eu/88u/dataset/mammalian-toxicokinetic-database-mamtkdb-1-0](http://data.europa.eu/88u/dataset/mammalian-toxicokinetic-database-mamtkdb-1-0)

European Commission, European Centre for Disease Prevention and Control,
'COVID-19 coronavirus data -- daily (up to 14 December 2020)', accessed
1 October 2021,
[http://data.europa.eu/88u/dataset/covid-19-coronavirus-data-daily-up-to-](http://data.europa.eu/88u/dataset/covid-19-coronavirus-data-daily-up-to-14-december-2020)
[14-december-2020](http://data.europa.eu/88u/dataset/covid-19-coronavirus-data-daily-up-to-14-december-2020)

'European database of suspected adverse drug reaction reports
(EudraVigilance),' European Commission, European Medicines Agency, 2015
(updated 10 January 2019),
<http://data.europa.eu/88u/dataset/suspected-adverse-drug-reaction-reports>

European Commission, Directorate-General for Internal Market, Industry,
Entrepreneurship and SMEs, 'Cosmetic ingredient database (Cosing) --
List of substances prohibited in cosmetic products,' 2016 (updated
14 December 2018),
[http://data.europa.eu/88u/dataset/cosmetic-ingredient-database-2-list-of-](http://data.europa.eu/88u/dataset/cosmetic-ingredient-database-2-list-of-substances-prohibited-in-cosmetic-products)
[substances-prohibited-in-cosmetic-products](http://data.europa.eu/88u/dataset/cosmetic-ingredient-database-2-list-of-substances-prohibited-in-cosmetic-products)

European Commission, Joint Research Centre, 'European soil database v2.0
(vector and attribute data),' 2021, accessed 1 October 2021,
[http://data.europa.eu/89h/jrc-](http://data.europa.eu/89h/jrc-esdac-1)
[esdac-1](http://data.europa.eu/89h/jrc-esdac-1)

Sevini, F. and Arnés-Novau, X., 'Export control handbook for chemicals,'
version 2021 edition, European Commission, Joint Research Centre (JRC),
2021, accessed 1 October 2021,
<http://data.europa.eu/89h/8f0d4fa2-110d-4c05-b2a8-ecd3addbf16b>

Nijs, W. and Ruiz, P., '01_JRC-EU-TIMES full model', European
Commission, Joint Research Centre, 2019,
<http://data.europa.eu/89h/8141a398-41a8-42fa-81a4-5b825a51761b>

'AMECO -- ECFIN annual macroeconomic database,' European Commission,
Directorate-General for Economic and Financial Affairs, (updated
5 November 2020), <http://data.europa.eu/88u/dataset/ameco>

European Commission, Directorate-General for Energy, 'Energy
modelling -- EU reference scenario 2016',
<http://data.europa.eu/88u/dataset/energy-modelling>

European Commission, Eurostat, 'Airport traffic data by reporting
airport and airlines' (avia_tf_apal), most recent data 1 September 2021,
[https://ec.europa.eu/eurostat/](https://ec.europa.eu/eurostat/databrowser/view/avia_tf_apal/default/table?lang=en)
[databrowser/view/avia_tf_apal/default/table?lang=en](https://ec.europa.eu/eurostat/databrowser/view/avia_tf_apal/default/table?lang=en)

European Commission, Eurostat, 'Total length of motorways' (ttr00002),
accessed 15 October 2021,
[https://ec.europa.eu/eurostat/databrowser/view/ttr00002/](https://ec.europa.eu/eurostat/databrowser/view/ttr00002/default/table?lang=en)
[default/table?lang=en](https://ec.europa.eu/eurostat/databrowser/view/ttr00002/default/table?lang=en)

European Commission, Eurostat, 'Real GDP growth rate -- volume'
(tec00115), updated 18 September 2021,
[https://ec.europa.eu/eurostat/databrowser/view/tec00115/](https://ec.europa.eu/eurostat/databrowser/view/tec00115/default/table?lang=en)
[default/table?lang=en](https://ec.europa.eu/eurostat/databrowser/view/tec00115/default/table?lang=en)

Publications Office of the European Union, 'Country named authority
list,' 2009 (updated 29 September 2021),
<http://data.europa.eu/88u/dataset/country>

*Source:* Publications Office, Jessop, P., *Data citation: A guide to
best practice*, 2022, <https://data.europa.eu/doi/10.2830/59387>
