Every day millions of people use data. They search, publish, reuse and
analyse. But very often, a major difficulty for the users is finding the
right data. Citing data can help to mitigate this.

In this section you will find information about our initiatives on data
citation.

