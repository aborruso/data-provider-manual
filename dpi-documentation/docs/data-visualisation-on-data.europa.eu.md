The visualisation tool uses as data source the files as provided by the
source. By doing so, it allows users to grasp the data quickly and to
understand if the dataset provides the information that the user is
looking for. The 'Preview' button is available for all datasets in CSV
format (more formats will be added over time). It can show the data in
tabular and graphical format without the need for downloading.
Geo-Visualisation is available for geodata WMS and GeoJSON files.

Potential errors: it is possible that the tool may not accept the
provided file format or that the files may be corrupted at the source.
The portal has no influence on the source files.
