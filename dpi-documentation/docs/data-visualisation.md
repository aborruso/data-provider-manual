Half of the human brain is dedicated to processing things visually.
Therefore, visualising information helps people perceive and make sense
of concepts and relationships in a complex modern world today. As a
result, effective visualisations can contribute to a significantly
better understanding of data and policies. They can achieve a
considerably higher impact on policymakers and citizens and contribute
to considerably higher political support.

The OP offers various services related to this competence, in the portal
data.europa.eu, organisation of conferences, webinars, training
material, etc.




