data.europa.eu provides the following APIs to read our metadata:

1. Search: https://data.europa.eu/api/hub/search/

2. SPARQL: https://data.europa.eu/sparql

3. Registry: https://data.europa.eu/api/hub/repo/

4. Use cases: https://data.europa.eu/en/export-use-cases

5. MQA: https://data.europa.eu/api/mqa/cache/

6. SHACL metadata validation: https://data.europa.eu/api/mqa/shacl/

