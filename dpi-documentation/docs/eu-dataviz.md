EU DataViz is an international conference on open data and data
visualisation addressing the needs of the public sector across the EU.
It is organised by the OP and the [first
edition](https://op.europa.eu/en/web/eudataviz2019) was in 2019,
followed by the [second edition](https://op.europa.eu/en/web/eudataviz)
in 2021 as part of the [EU Open Data
Days](https://op.europa.eu/en/web/euopendatadays/).

In a nutshell, the conference addressed how to translate complex issues
and knowledge into digestible and relevant language that resonates with
EU citizens and policymakers. Additionally, it helps to explore and
analyse data to develop better policies.

Featuring expert speakers, EU DataViz 2021 provided an overview of
innovative techniques and best practices used in both the private and
the public sectors, offering the participants valuable insights into
open-data and data-visualisation techniques and practices. The
recordings, programme and presentations are available [on the
website](https://op.europa.eu/en/web/eudataviz/programme).

Watch the video on EU Open Data Days and discover the EU DataViz event:
<https://www.youtube.com/watch?v=_PGRfvsNooQ>

## Data visualisation on data.europa.eu

Following the organisation of the first EU DataViz (2019), we created
[one section under data.europa academy on data
visualisation](https://data.europa.eu/en/academy/data-visualisation) to
offer deeper understanding on this topic, its potential benefits and the
necessary techniques to visualise open data.

You can watch the EU DataViz webinars, inviting speakers to present
their best practices and innovative techniques regarding data
visualisation.
