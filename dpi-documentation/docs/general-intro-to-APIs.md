In digital form, a machine can exchange data. To provide a seamless
interaction for data exchange, APIs are defined by developers of such a
machine. Those APIs can guide humans and machines on how to interact and
understand the exchanged data. For each API call, the requesting service
is defined as the client and the service providing data exchange is
defined as the server. In case of a human, the browser or a dedicated
application for API calls would represent the humans as the client.
Beyond those basic principles, it is important to understand the
concepts of HTTP, URL, data representations formats and predefined
communication methods.

### HTTP

The "Hypertext Transfer Protocol" (HTTP) allows to exchange hypertext
between machines. Unlike standard text, hypertext can be non-linear and
can contain links to other text. Nevertheless, hypertext can easily be
read and understood by humans. HTTP is not only indispensable for
browsing the Web, but has also established as a standard for modern
APIs. The payload via HTTP can be information of any kind, e.g.
executable code or data. Beyond payload, HTTP contains meta information
in their headers, e.g. information about the communication session. To
provide a secured communication HTTP is combined "Transport Layer
Security" (TLS). Together they form HTTPS, a secure version of HTTP.
HTTPS is crucial to provide a private conversion against external
entities.

###  URL

A "Uniform Resource Locator" (URL) is form of an address on the Web. In
the context of APIs, a URL can address entry points, functions or
resources provided by a server. In general, URLs are used to varying
degrees depending on the API type. Some APIs use URLs only to address an
entrypoint, others use path and query parameters to provide information
to the server.

###  Data representation format

In addition to the communication protocol of the API and where the API
is located, an API can define the representation of the data payload.
For this, data may be represented as plain text, structed and encoded
data. In practice, data representation formats are used for structured
data that are machine-readable to a certain degree, e.g. XML, JSON or
YAML.

###  Predefined methods

When it comes to the management of resource, APIs typically provide
create, read, update and delete operations (CRUD). For this, the HTTP
protocol has predefined methods that can be used to define an API.

-   POST, to create a resource

-   GET, to read a resource

-   PUT, to create or update a resource

-   PATCH, to update a resource

-   DELETE, to delete a resource

## What is OpenAPI and why is it relevant

All APIs on data.europa.eu are documented via OpenAPI. OpenAPI provides
a human and machine-readable language agnostic interface description
that enables everyone to use the APIs without having to investigate the
source code. By using the specified syntax of OpenAPI, tools can be used
to automatically create accessible and human readable documentations of
our APIs. The screenshot below showcases a human readable documentation
of the MQA API.

![](./images/media/image70.png)

It tells the user how to retrieve global quality measurements from the
MQA. It gives the user a short description of the context of the
provided features and concrete explanations of all required and optional
parameters that can be used when using the API. In the case at hand the
parameter "filter" can be used, and it is explained that the value of
the parameter must be one of the following: "findability",
"accessibility", "interoperability", "reusability",
"contextuality", "score"

The right-hand side gives a concrete URL to request for testing the API.
Please note that the example URL is not using any parameters. These must
be added manually. Below the example URL an example response is given.
