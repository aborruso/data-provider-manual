There are four ways of searching the portal: manual search, advanced
search, SPARQL search and graph search.

### Manual search

####  Boolean operators

###### General Remarks

You can search for datasets containing one or more keywords by typing
the keywords into the search field and then clicking on the search
button. When entering more than one search term it is also possible to
use the logical operators (connectors) **AND, OR** as well as
**parentheses ()**. The concepts behind and rules for using logical
operators and parentheses are briefly explained in the following
sections.

###### Concepts of logical operators and parentheses

Logical Operator OR

The OR operator can be used in order to

-   connect two or more similar concepts (synonyms)

-   broaden your results, telling the database that ANY of your search
    terms can be present in the resulting dataset

Example: *population OR education OR science*

All three circles represent the result set for this search. It is a big
set because any of those words are valid using the OR operator.

![](./images/media/image38.png)

Logical Operator AND

The AND operator is used to

-   find sources containing two or more ideas

-   narrow the search

The database will only retrieve items containing both keywords. The AND
operator can be used multiple times in one query.

Example: *population AND education AND science*

![](./images/media/image39.png)

Parantheses ()

Parentheses is used to

-   perform a more complex search using both AND and OR by placing
    parentheses around synonyms

-   save time by searching multiple synonyms at once

Example: *(environment OR nature) AND (refurbish OR reuse)*

This cuts down on having to perform multiple searches for the
combinations on keywords.

![](./images/media/image40.png)

###### Basic rules for using AND, OR operators and parentheses ()

The **OR** is implied: search function automatically puts an OR in
between your search terms. It means that the search for *population OR
education OR science* gets the same matches as the search for
*population education science*.

![](./images/media/image41.png)

![](./images/media/image42.png)

Always enter **AND, OR** operators in uppercase letters.

![](./images/media/image43.png)

Never translate **AND, OR** operators into any other language. It does
not matter in what language your keywords are \-- the operators always
have to be **AND, OR**.

![](./images/media/image44.png)

![](./images/media/image45.png)

Please keep in mind to always close **parentheses ()**. The combination
of logical operators with an odd number of **parentheses** in the query
leads to incorrect results in the search.

![](./images/media/image46.png)

![](./images/media/image47.png)

