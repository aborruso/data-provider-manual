# Who we are

[The portal](http://data.europa.eu/) is a **central point of access** to
European open data from international, European Union, national,
regional, local and geodata portals. It consolidates the former [EU Open
Data Portal](https://en.wikipedia.org/wiki/EU_Open_Data_Portal) and the
European Data Portal.

The portal is intended to:

1.  give access and foster the reuse of European open data among
    citizens, business and organisations;

2.  promote and support the release of more and better-quality metadata
    and data by the EU's institutions, agencies and other bodies, and
    European countries;

3.  educate citizens and organisations about the opportunities that
    arise from the availability of open data.

![Diagram Description automatically generated](./images/media/image1.jpeg)

The two former portals EU Open Data Portal and European Data Portal,
launched respectively in 2012 and 2015, were originally established on
the basis of
[Directive 2003/98/EC](https://en.wikipedia.org/wiki/Directive_on_the_re-use_of_public_sector_information)
to promote accessibility to and the reuse of public sector information.
The successor directives Directive 2013/37/EU and [Directive
(EU) 2019/1024](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32019L1024&from=EN)
confirmed and extended its action. It invites all the EU Member States
to publish their public data resources, such as open datasets, and to
make them accessible to the public whenever possible.

Currently in its third iteration, the portal merges the activities of
the European Data Portal (which focused exclusively on EU Member States
and other European countries) and of the EU Open Data Portal (which
served data from the EU institutions, agencies, and bodies) into one.

It is funded by the EU and managed by the [Publications Office of the
European
Union](https://en.wikipedia.org/wiki/Publications_Office_of_the_European_Union).
The [Directorate-General for Communications Networks, Content and
Technology](https://en.wikipedia.org/wiki/Directorate-General_for_Communications_Networks,_Content_and_Technology)
of the [European
Commission](https://en.wikipedia.org/wiki/European_Commission) is
responsible for the implementation of EU open data policy, in
collaboration with the project's management.

![](./images/media/image2.jpg)


!!! information
    You can download and share the latest version of this documentation in the PDF format: [Download](pdf/documentation_data-europa-eu_V1.0.pdf)


##  Our mission, vision and values

The mission, vision and values of data.europa.eu are part of those from
the Publications Office of the European Union (OP) with respect to open
data.

**OP's mission.** The Publications Office of the European Union is the
official provider of publishing services to all EU institutions, bodies,
and agencies. As such, it is a central point of access to EU law,
publications, open data, research results, procurement notices and other
official information. Its mission is to support EU policies and ensure
that this broad range of information is available to the public as
accessible and reusable data to facilitate transparency, economic
activity, and the diffusion of knowledge.

**OP's vision.** A well-informed EU, empowered by timely and effective
access to trustworthy information and knowledge and benefiting from all
the opportunities this brings to society and the economy.

**OP's values.**

1.  Transparency -- we facilitate transparency throughout the policy
    cycle of the EU institutions to enhance evidence-based
    decision-making, accountability, civic participation, and democracy.

2.  Trustworthiness -- we strive to ensure that the content we provide
    is accurate and reliable so that citizens trust the EU as a provider
    of information.

3.  Accessibility -- we believe access to information is a human right
    that all citizens should enjoy regardless of language, culture,
    disability, social status, location, technology, or the way they
    understand information.

4.  Service orientation -- we are committed to continuously improving
    our services to both our institutional stakeholders and EU citizens
    because we want to contribute to the European project in the best
    possible way.

![Diagram Description automatically
generated](./images/media/image3.jpeg)

 

## Better access, enhanced transparency and use

The portal is aimed at improving the accessibility to open data and
promoting its use by public administrations, research centres, citizens,
businesses, and any other interested organisation. It enhances the
transparency of European administrations and educates about the
opportunities arising from open data.

![Diagram, engineering drawing Description automatically
generated](./images/media/image4.jpeg)

 

## Translated metadata catalogue

The portal is a metadata catalogue. To foster the comparability of data
published across borders, it presents metadata references in a familiar
format ([Data Catalogue Vocabulary (DCAT) application profile for data
portals in
Europe](https://joinup.ec.europa.eu/asset/dcat_application_profile/description)),
using Resource Description Framework (RDF) technology. It provides
translations of metadata descriptions in all [24 official EU
languages](https://europa.eu/european-union/about-eu/eu-languages_en)
using machine-translation technologies
([eTranslation](https://webgate.ec.europa.eu/etranslation/public/welcome.html)).

In some situations, metadata machine-based translation might not be as
efficient as human translation.

![Diagram Description automatically
generated](./images/media/image5.jpeg)


## From raw data to services

The metadata catalogue of datasets can be explored through a search
engine ([data tab](https://data.europa.eu/data/datasets)), through a map
for geospatial data or through a [SPARQL
endpoint](https://data.europa.eu/data/sparql?locale=en) and [API
endpoint](https://data.europa.eu/api/hub/search/).

The 'data providers' (EU institutions, agencies and EU bodies, Member
States, and other European countries) are autonomous in publishing their
metadata (which gives you access to their data) in
[data.europa.eu.](https://data.europa.eu/en) The portal also publishes
datasets of other European countries and organisations beyond the EU.
The portal is updated when new datasets and content are available. 

In addition to the vast collection of public datasets, most of them
published in open formats, we offer additional contents and related
services:

-   training materials on open data in the [data.europa
    academy](https://data.europa.eu/en/academy);

-   studies such as [*Open Data
    Maturity*](https://data.europa.eu/en/impact-studies/open-data-maturity)
    or [*The Economic Impact of Open
    Data*](https://data.europa.eu/en/impact-studies/open-data-impact);

-   [news](https://data.europa.eu/en/news-events/news) and [data
    stories](https://data.europa.eu/en/news-events/datastories);

-   a [metadata quality
    dashboard](https://data.europa.eu/mqa/?locale=en) assessing metadata
    quality against various findable, accessible, interoperable, and
    reusable (FAIR) indicators;

-   our social media channels
    ([Twitter](https://twitter.com/EU_opendata),
    [LinkedIn](https://www.linkedin.com/company/publications-office-of-the-european-union),
    [YouTube](https://www.youtube.com/c/PublicationsOffice),
    [Facebook](https://www.facebook.com/data.europa.eu));

-   a list of events with the most relevant events taking place;

-   a quarterly
    [Newsletter](https://data.europa.eu/en/news-events/newsletter).

## Open data in the European Commission

Several policy and legal developments on EU open data have taken place
since 2003.

-   **2003** --
    [Directive 2003/98/EC](https://eur-lex.europa.eu/legal-content/en/ALL/?uri=CELEX%3A32003L0098)
    on the re-use of the public sector information (PSI) directive.

-   **2011** -- [Commission
    Decision (2011/833/EU)](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32011D0833)
    on the reuse of Commission documents, established the basis for the
    former EU Open Data Portal, operated by the [Publications Office of
    the EU](https://op.europa.eu/en/home) (2012--2021).

-   **2012** -- launch of the EU Open Data Portal, central point of
    access to open data from EU institutions, agencies, and bodies.

-   **2013** --
    [Directive 2013/37/EU](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex%3A32013L0037)
    amending Directive 2003/98/EC on the re-use of public sector
    information.

-   **2015** -- launch of the European Data Portal publishing data from
    national, regional, and local open data portals.

-   **2019** --
    [Directive 2019/1024](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32019L1024)
    on open data and the re-use of public sector information recasts
    Directives [2003/98/EC](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32003L0098&from=en)
    (PSI directive) and
    [2013/37/EU](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32013L0037&from=EN)
    setting up a legal framework of minimum requirements for Member
    States regarding the accessibility of public data resources. 

-   **2020** -- [Communication on a European strategy for
                                                                    data](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A52020DC0066)
                                                                    (COM/2020/66) aiming at creating a single market for data to ensure
                                                                    Europe's global competitiveness.

-   **2021** -- The European Data Portal (data from the EU Member States
    and other European countries) and the EU Open Data Portal (data from
    the EU institutions, agencies, and bodies) were consolidated into
    the data.europa.eu portal.

![](./images/media/image6.jpg)

## European data strategy

The [European data
strategy](https://ec.europa.eu/info/strategy/priorities-2019-2024/europe-fit-digital-age/european-data-strategy)
fosters a single market for data for the benefit of businesses,
researchers, and public administrations. The portal will continue to
play a crucial role in supporting data accessibility and
interoperability, facilitating its use and value creation.

![Diagram Description automatically
generated](./images/media/image7.jpeg)



## European open data space

The European open data space is an essential element of the single
market for data -- an EU-wide interoperable data space that will enable
the development of new products and services based on public data, and
industrial and scientific applications. It focuses on the implementation
of EU open data and reuse policies under the legal acts adopted by the
EU institutions. In 2021 the Publications Office of the EU was working
on all four European open data space building blocks and their
objectives:

-   providing a comprehensive catalogue of open data and citizen-centric
    reuse services;

-   improving the interlinking and interoperability of open data with
    other sources of public-sector information, such as legislation,
    publications, and digital content;

-   fostering the use of data from EU content through the organisation
    of [EU Datathon](https://op.europa.eu/en/web/eudatathon)
    competitions and data visualisation events;

-   contributing to the implementation of data governance and policies
    across the EU institutions.

![Diagram Description automatically
generated](./images/media/image8.jpeg)

