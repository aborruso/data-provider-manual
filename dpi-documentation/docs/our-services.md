Our services are focused around three main spheres.

-   Data publishers -- any person or entity that gives access and
    distributes data to the public. To ensure the reliability of the
    published resources in the portal, the data publishers of the portal
    are official representatives from supranational, national and local
    public administration.

-   Data users -- any person or entity who accesses and consumes data
    for any purpose. These include a wide range of data users who visit
    the portal for different purposes: non-governmental organisations,
    international organisations, private sector, academia, students,
    etc.

-   Data literacy -- the ability to read, understand, create, and
    communicate data as information. Much like literacy as a general
    concept, data literacy focuses on the competencies involved in
    working with data.

![](./images/media/image9.jpg)

If you are looking for data, you can find more than one million datasets
from all over Europe and beyond, including publications, learning
material and the latest news about open data.

The portal enables users to discover, explore, link, download and easily
reuse data for commercial or non-commercial purposes, subject to the
licence chosen by the data provider through a common, homogenised
metadata catalogue. From the portal, users can access data published on
the websites of the various data providers.

Semantic technologies offer additional functionalities. The metadata
catalogue can be searched via an interactive search engine (data tab)
and through [SPARQL](https://en.wikipedia.org/wiki/SPARQL) queries
(SPARQL search tab).

The portal offers many additional features for exploring open data, such
as statistics, use cases, open data news, data stories and country
insights. Additionally, it offers an events calendar and a newsletter to
help users stay up to date with the developments in the open data world.

The interface is available in the 24 official languages of the EU.
Whenever the metadata is not available in any of the languages, it is
machine translated from the original language into the others.


