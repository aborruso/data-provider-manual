The portal publishes many publications ranging from the yearly *Open
Data Maturity* report to specific data stories and open data news.

The data.europa.eu portal offers news and social media coverage on open
data topics, with the purpose of educating and informing citizens about
the opportunities that using and reusing public data resources can
bring. It also hosts the data.europa academy, a knowledge hub for open
data-related topics, and publishes a quarterly newsletter.

The 'country insights' section of the portal provides insights into the
status of open data in European countries. The section includes
information about countries' national open data portal, the level of
open data maturity, use cases, catalogues, events, reports, news,
webinars and interviews about open data.

The 'use cases' section offers a catalogue that includes hundreds of use
cases from both publishers and people who reuse open data.

The portal is linked to other activities that demonstrate the potential
of open data, such as the EU Datathon (an annual open data competition),
the EU Open Data Days (a worldwide conference about open data and data
visualisation) and different online seminar series (EU DataViz webinars,
EU Open Data Explained webinars and data.europa academy webinars).

The portal also offers documentation on general recommendations for
topics like data citation and data quality.

Finally, the portal researches and documents the impact of open data in
reports such as the 2020 report on *The Economic Impact of Open Data --
Opportunities for value creation in Europe*. The study forecasts the
open data market size and employment growth for 2025. The impact is
exemplified by efficiency gains and cost savings due to open data.

### Open data maturity

The study on open data maturity serves as benchmark to gain insights
into the development of European countries in the field of open data.
This yearly assessment, started in 2015, presents the overview to better
understand the countries level of maturity on open data. The study
captures their progress over time and areas of improvement and
benchmarks them against other countries. The report assesses the
maturity against four dimensions:

        1.  policy
        2.  portal
        3.  impact
        4.  quality

It also provides an overview of best practices implemented across Europe
and a set of recommendations tailored to the level of maturity and
characteristics of each group. Every year, the data is collected through
a questionnaire sent to national open data representatives working in
collaboration with the European Commission and the Public Sector
Information Expert Group.

### Data stories

Data stories are a set of monthly articles that inspire and inform you
about open data trending initiatives. They highlight examples of good
practices and innovative techniques across the world on open data.

### Studies

The portal commits to open data research and learning as key milestones
to keep updating in this constantly changing data ecosystem. This
section provides a full collection of the research developed on several
topics.

### Data.europa academy

The [data.europa academy](https://data.europa.eu/en/academy) is your
knowledge hub for open data. It helps you to become more knowledgeable
about open data and become more data literate. To help you find the
right course, it is structured along four themes according to the open
data maturity assessment: policy, impact, technology and quality.

It provides complete and up-to-date knowledge about tools and how to
publish high-quality open data, whether you are an expert or just
getting started.

### News

In the news section of data.europa.eu, you can find short articles about
the latest activities, events, projects, initiatives, studies, trends
and use cases related directly or indirectly to open data in Europe and
beyond. If you would like your event, project, or initiative to be
featured in our news section, [let us know via the contact
form.](https://data.europa.eu/en/feedback/form)

### Calendar

The events calendar features virtual and in-person events related
directly and indirectly to open data in Europe and beyond. If you would
like your event to be featured in this section of the portal, [let us
know via the contact form.](https://data.europa.eu/en/feedback/form)

### EU Open Data Days

The [EU Open Data Days](https://op.europa.eu/en/web/euopendatadays) in
2021, was the first-ever event of its kind and took place from 23 to
25 November 2021. It comprised [EU
DataViz](https://op.europa.eu/en/web/eudataviz), an international
conference on open data and data visualisation, followed by the finals
of [EU Datathon](https://op.europa.eu/en/web/eudatathon), the annual
open data competition.

The EU Open Data Days were designed to be relevant to all open data
stakeholders and data users, with a special emphasis on the needs of the
EU's public sector. The event attracted well over 2 000 registrations
from data enthusiasts, data visualisation experts and solution-seekers.
On the stage, different sectors of the society were represented, with
more than 70 speakers coming from all around the world to bring their
expertise for public administrations.

The EU Open Data Days were organised by the OP, with the active support
of over 60 partners, representing the data providers from EU
institutions and agencies, European national open data portals and
national statistical offices, the key digital players in Luxembourg.

You can access the [recordings and presentations](https://op.europa.eu/en/web/euopendatadays/programme)
online.

### EU Datathon

EU Datathon is an annual open data competition organised by the OP since 2017. The competitions are organised to create **new value for citizens through innovation and promote the use of open data** available on [the portal](https://data.europa.eu/en).

EU Datathons gather teams of people from all over Europe to stimulate
innovation, creating a bridge between citizens and the EU's
administration. As a result, new applications are developed to improve
existing services or create completely new ones. The competitions have
also demonstrated that potential users do need these initiatives for
visibility purposes before potential business partners.

Open data demonstrated in every edition how powerful it could be, in
every sector of society. It showed us how it could help in the creation
of start-ups, innovative products and services; it promotes transparency
in the public sector, addresses current societal challenges, boosts
economic growth and accelerates scientific progress.

Every EU Datathon competition showed innovation when teams tackled
different challenges, such as helping to achieve the digital single
market (e.g. [Tenderlake](https://www.tenderlake.com/) and
[C4P.io](http://www.c4p.io/)), making legislation more interoperable
(e.g. [The Smartfiles Network](https://smartfiles.lereto.at/search) and
[Lexparency](https://lexparency.org/)), offering innovative technology
relating to medicines (e.g.
[Medicatio](https://medicatio-next.firebaseapp.com/) and [Open Food
Facts](https://world.openfoodfacts.org/)) or tackling climate change
([ODCCI](http://eudt2019.403.io/)) through a platform which optimises
irrigation to reduce water waste (e.g. [Chloe irrigation
system](https://play.google.com/store/apps/details?id=com.chloeirrigation.chloe)).

These examples, among others, uncover how accessible and reusable data
can help to build a better future.

EU Datathons are organised in **collaboration with partners in EU
institutions and Member States** to address domains of primary
importance for citizens, the public sector and businesses: legislation,
medications, food safety and public procurement, among others.

This interinstitutional collaboration highlights the common goals to be
closer to citizens and support innovative start-ups. Additionally, by
facilitating cooperation between public entities and the private sector,
EU Datathons generate innovative ideas for open government, offering a
platform for all sides to exchange information and learn.

Moreover, it is a new, more direct way of interaction between citizens,
innovative youth and the EU administration and it can generate a
positive atmosphere around the Commission and the EU institutions in
general.

### Newsletter

The [portal
newsletter](https://data.europa.eu/en/news-events/newsletter) is sent to
subscribers quarterly. It provides an overview of the recent and
upcoming events and activities of the portal, along with the most
important news, events, and developments in the open data domain in
Europe. To subscribe to the newsletter, [sign up
here](https://data.europa.eu/en/newsletter). To unsubscribe, click on
the 'Unsubscribe from this newsletter' link at the bottom of the
newsletter email.
