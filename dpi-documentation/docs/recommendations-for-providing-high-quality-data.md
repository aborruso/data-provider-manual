Data needs to be carefully prepared before publication. Preparation is
an interactive and agile process used to explore, combine, clean and
transform raw data into curated, high-quality datasets. This process
consists of six different phases, illustrated in the image below.

![](./images/media/image71.png)

*Source:* Publications Office, *Data.europa.eu Data Quality Guidelines*,
2022, <https://data.europa.eu/doi/10.2830/333095>

General tip: make use of tooling and create a data management plan.

**Best practices for providing high-quality data (findability,
accessibility, interoperability, reusability)**

![](./images/media/image72.png)

*Source:* based on Publications Office, *Data.europa.eu Data Quality
Guidelines*, 2022, <https://data.europa.eu/doi/10.2830/333095>

### CSV

![data-quality-csv](./images/media/data-quality-csv.png)

### XML

![data-quality-xml](./images/media/data-quality-xml.png)

### RDF

![data-quality-rdf](./images/media/data-quality-rdf.png)

### JSON

![data-quality-json](./images/media/data-quality-json.png)

### APIs
![data-quality-api](./images/media/data-quality-api.png)
