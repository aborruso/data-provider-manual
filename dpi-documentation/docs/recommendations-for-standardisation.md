![data-quality-api](./images/media/data-quality-data-enrichment.png)

## Recommendations for documenting data
![data-quality-api](./images/media/data-quality-documenting-data.png)

## Recommendations for improving the 'openness level'
![data-quality-api](./images/media/data-quality-openness-level.png)


### File formats and their achievable openness level

![](./images/media/image73.png)

\* Strictly according to the five-star model, this format would have to
be rated with three stars, since the data may well be designed to be
machine readable. However, we only give one star because this format was
not originally intended to represent machine-readable but human-readable
content. Representing machine-readable content in this format does not
meet best practice and is therefore not recommended by the authors.

*Source:* based on Publications Office, *Data.europa.eu Data Quality
Guidelines*, 2022, <https://data.europa.eu/doi/10.2830/333095>
