All APIs on data.europa.eu are documented via OpenAPI. OpenAPI provides
a human and machine-readable language agnostic interface description
that enables everyone to use the APIs without having to investigate the
source code. By using the specified syntax of OpenAPI, tools can be used
to automatically create accessible and human readable documentations of
our APIs. The screenshot below showcases a human readable documentation
of the MQA API.

![](./images/media/image70.png)

It tells the user how to retrieve global quality measurements from the
MQA. It gives the user a short description of the context of the
provided features and concrete explanations of all required and optional
parameters that can be used when using the API. In the case at hand the
parameter "filter" can be used, and it is explained that the value of
the parameter must be one of the following: "findability",
"accessibility", "interoperability", "reusability",
"contextuality", "score"

The right-hand side gives a concrete URL to request for testing the API.
Please note that the example URL is not using any parameters. These must
be added manually. Below the example URL an example response is given.
